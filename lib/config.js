/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const { createMessageStore } = require('gearshaft/message-store/postgres')
const { createPostgresGateway } = require('gearshaft/postgres-gateway')
const { createWriter } = require('gearshaft/messaging')
const pino = require('pino')

const AccountComponent = require('./account-component')

function createConfig ({ env }) {
  const logConfig = {
    name: env.appName,
    level: env.logLevel
  }
  const log = pino(logConfig)

  const postgresOptions = {
    type: 'postgres',
    host: env.messageStoreHost,
    user: env.messageStoreUser,
    database: env.messageStoreDatabase
  }

  const postgresGateway = createPostgresGateway(postgresOptions)
  postgresGateway.on('error', err => {
    // prevent exposing postgres credentials and clogging up logs
    // https://github.com/brianc/node-postgres/issues/1568
    delete err.client

    log.error({ err }, 'postgres connection error')
  })

  const messageStore = createMessageStore({ log, postgresGateway })
  const write = createWriter({ log, messageStore })

  const accountComponent = AccountComponent({ log, messageStore, write })

  return {
    env,
    log,
    messageStore,
    postgresGateway,
    write,
    accountComponent
  }
}

module.exports = createConfig
