class Account {
  constructor () {
    this.id = null
    this.openedAt = null
  }

  opened () {
    return !!this.openedAt
  }
}

module.exports = Account
