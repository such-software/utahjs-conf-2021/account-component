const { createEntityStore } = require('gearshaft/entity-store')

const Account = require('./account')
const Projection = require('./projection')

function build ({ messageStore }) {
  return createEntityStore({
    category: 'account',
    entity: Account,
    projection: Projection,
    messageStore
  })
}

module.exports = build
