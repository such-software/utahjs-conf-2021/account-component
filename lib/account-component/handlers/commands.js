const { entityStreamName } = require('../streams')

const Open = require('../messages/commands/open')
const Opened = require('../messages/events/opened')

function build ({ log, store, write }) {
  async function handleOpen (open) {
    const accountId = open.accountId

    const [account, { version }] = await store.fetchRecord(accountId)

    if (account.opened()) {
      log.info({ accountId }, 'Account already opened - ignoring')

      return
    }

    const opened = Opened.follow(open)

    const streamName = entityStreamName(accountId)

    write.initial(opened, streamName)
  }

  function register (handle) {
    handle(Open, handleOpen)
  }

  return {
    handleOpen,
    register
  }
}

module.exports = build
