const Controls = require('./controls')
const Streams = require('./streams')
const Store = require('./store')
const Handlers = {
  Commands: require('./handlers/commands')
}
const Consumers = {
  Commands: require('./consumers/commands')
}
const Projection = require('./projection')

function build ({ log, messageStore, write }) {
  const store = Store({ messageStore })
  const commandHandlers = Handlers.Commands({ log, store, write })
  const commandConsumer = Consumers.Commands({
    log,
    registerHandlers: commandHandlers.register,
    messageStore
  })

  return {
    store,
    commandHandlers,
    commandConsumer
  }
}

module.exports = exports = build

exports.Handlers = Handlers
exports.Controls = Controls
exports.Streams = Streams
exports.Projection = Projection
