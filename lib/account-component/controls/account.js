const Account = require('../account')
const Id = require('./id')
const Time = require('./time')

module.exports = {
  New: {
    example () {
      return new Account()
    }
  },

  Opened: {
    example (accountId = Id.example()) {
      const account = new Account()

      account.accountId = accountId
      account.openedAt = Time.Effective.raw()

      return account
    }
  }
}
