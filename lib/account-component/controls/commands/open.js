const Open = require('../../messages/commands/open')
const Id = require('../id')
const Time = require('../time')

module.exports = {
  example () {
    const fields = {
      accountId: Id.example(),
      customerId: Id.example(),
      openedAt: Time.Effective.example()
    }

    return Open.create(fields)
  }
}
