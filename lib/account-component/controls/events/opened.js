const Opened = require('../../messages/events/opened')
const Id = require('../id')
const Time = require('../time')

module.exports = {
  example () {
    const fields = {
      accountId: Id.example(),
      customerId: Id.example(),
      openedAt: Time.Effective.example()
    }

    return Opened.create(fields)
  }
}
