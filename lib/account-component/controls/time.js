module.exports = {
  example () {
    return '2021-01-01T17:00:00'
  },

  Effective: {
    example () {
      return this.raw().toISOString()
    },

    raw () {
      return new Date('2021-01-01T00:00:01')
    }
  },

  Processed: {
    example () {
      return this.raw().toISOString()
    },

    raw () {
      return new Date('2021-01-01T00:00:11')
    }
  }
}
