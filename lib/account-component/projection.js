const { createEntityProjection } = require('gearshaft/entity-projection')

const Opened = require('./messages/events/opened')

function applyOpened (account, opened) {
  account.id = opened.accountId
  account.openedAt = opened.openedAt
}

function projection (register) {
  register(Opened, applyOpened)
}

module.exports = exports = createEntityProjection(projection)
exports.applyOpened = applyOpened
