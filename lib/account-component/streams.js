const { createCategory } = require('gearshaft/message-store')

module.exports = createCategory('account')
