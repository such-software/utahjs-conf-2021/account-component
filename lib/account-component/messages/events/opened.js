const { Message } = require('../../../messaging')

class Opened extends Message {}

module.exports = Opened
