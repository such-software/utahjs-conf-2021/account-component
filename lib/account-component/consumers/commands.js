const { createConsumer } = require('gearshaft/consumer')

const { commandCategory } = require('../streams')

function build ({ log, registerHandlers, messageStore }) {
  return createConsumer({
    category: commandCategory(),
    log,
    messageStore,
    name: 'Registration.Command',
    registerHandlers
  })
}

module.exports = build
