const createConfig = require('./config')
const env = require('./env')

const config = createConfig({ env })

function start () {
  config.components.forEach(s => s.start())

  console.log('Started version', env.version)
}

module.exports = {
  config,
  start
}
