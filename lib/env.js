const colors = require('colors/safe')
const dotenv = require('dotenv')

const packageJson = require('../package.json')

const envResult = dotenv.config()

if (envResult.error) {
  // eslint-disable-next-line no-console
  console.error(
    `${colors.red('[ERROR] env failed to load:')} ${envResult.error}`
  )

  process.exit(1)
}

function requireFromEnv (key) {
  if (!process.env[key]) {
    // eslint-disable-next-line no-console
    console.error(`${colors.red('[APP ERROR] Missing env variable:')} ${key}`)

    return process.exit(1)
  }

  return process.env[key]
}

module.exports = {
  appName: 'UtahJS Conf 2021 - account-component',
  env: requireFromEnv('NODE_ENV'),
  version: packageJson.version,
  messageStoreHost: requireFromEnv('MESSAGE_STORE_HOST'),
  messageStoreUser: requireFromEnv('MESSAGE_STORE_USER'),
  messageStoreDatabase: requireFromEnv('MESSAGE_STORE_DATABASE'),
  logLevel: requireFromEnv('LOG_LEVEL')
}
