const test = require('blue-tape')

const {
  Controls,
  Handlers: { Commands }
} = require('../../../../lib/account-component')
const { TestConfig } = require('../../../automated-init')

test('Open - Opened', async t => {
  const open = Controls.Commands.Open.example()
  const { log, store, write } = TestConfig()

  const handlers = Commands({ log, store, write })

  await handlers.handleOpen(open)

  write.assertOnlyWriteInitial(`account-${open.accountId}`, opened => {
    t.equals(opened.accountId, open.accountId, 'Account ID matches')
    t.equals(opened.customerId, open.customerId, 'Customer ID matches')
    t.equals(opened.openedAt, open.openedAt, 'openedAt matches')
  })
})
