const test = require('blue-tape')

const {
  Controls,
  Handlers: { Commands }
} = require('../../../../lib/account-component')
const { TestConfig } = require('../../../automated-init')

test('Open - Ignored', async t => {
  const { log, store, write } = TestConfig()

  const open = Controls.Commands.Open.example()
  const account = Controls.Account.Opened.example()
  const version = 0

  store.add(open.accountId, account, version)

  const handlers = Commands({ log, store, write })

  await handlers.handleOpen(open)

  write.assertNoWrites(`account-${open.accountId}`)
})
