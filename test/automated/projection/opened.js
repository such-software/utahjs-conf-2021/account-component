const test = require('blue-tape')

const {
  Controls,
  Projection: { applyOpened }
} = require('../../../lib/account-component')

test('Projection = Opened', t => {
  const account = Controls.Account.New.example()
  const opened = Controls.Events.Opened.example()

  t.false(account.opened(), 'Account is not yet opened')

  applyOpened(account, opened)

  t.assert(account.opened(), 'Account is opened')

  t.end()
})
