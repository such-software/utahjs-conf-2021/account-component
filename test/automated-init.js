const test = require('blue-tape')
const { createEntityStoreSubstitute } = require('gearshaft/entity-store')
const { createWriterSubstitute } = require('gearshaft/messaging')

const { config } = require('../lib')
const Account = require('../lib/account-component/account')
const { createTestLog: TestLog } = require('./automated/test-log')

test.onFinish(() => {
  //await config.postgresGateway.end()
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

function TestConfig () {
  const log = TestLog()
  const store = createEntityStoreSubstitute({
    entity: Account
  })
  const write = createWriterSubstitute()

  return { log, store, write }
}

module.exports = {
  config,
  TestConfig
}
