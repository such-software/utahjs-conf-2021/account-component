const { startHost } = require('gearshaft/host')

const { config } = require('../lib')

config.log.info('host is starting')

const host = startHost(host => {
  host.register(config.accountComponent.commandConsumer)
})

host.on('stopped', async () => {
  await config.postgresGateway.end()
  config.log.info('host has stopped')
})
